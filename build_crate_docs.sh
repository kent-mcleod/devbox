#!/usr/bin/env bash

cratename() {
    case $1 in
        rust-sel4) echo sel4;;
        sel4-start) echo sel4_start;;
        sel4-sys) echo sel4_sys;;
        sel4-alloc) echo sel4_alloc;;
        acpica-sys) echo acpica_sys;;
        *) echo $1;;
    esac
}

mkdir -p upstream_docs
d=$(pwd)/upstream_docs

mkdir -p $d/i686-sel4-robigalia
mkdir -p $d/x86_64-sel4-robigalia
mkdir -p $d/arm-sel4-robigalia

index=$d/index.html
truncate -s 0 $index

cp meta/docs/style.css $d/
cp meta/docs/style.css $d/i686-sel4-robigalia
cp meta/docs/style.css $d/x86_64-sel4-robigalia
cp meta/docs/style.css $d/arm-sel4-robigalia

echo '<!doctype html> <html><head><meta charset="utf-8"><body>' >> $index
cat < meta/docs/template-before.html >> $index
echo "<p>Crates documented here:</p><ul>" >> $index
for repo in pci rust-sel4 sel4-start sel4-sys virtio acpica-sys tempo; do
    pushd $repo
    mkdir -p $d/$repo
    echo "<li>$(cratename $repo) <a href=\"https://doc.robigalia.org/arm/$(cratename $repo)\">arm</a> <a href=\"https://doc.robigalia.org/i686/$(cratename $repo)\">i686</a> <a href=\"https://doc.robigalia.org/x86_64/$(cratename $repo)\">x86_64</a></li>" >> $index
    echo '<!doctype html> <html><head><meta charset="utf-8"><body>' >> $d/$repo/index.html
    echo "$(cratename $repo) <a href=\"https://doc.robigalia.org/arm/$(cratename $repo)\">arm</a> <a href=\"https://doc.robigalia.org/i686/$(cratename $repo)\">i686</a> <a href=\"https://doc.robigalia.org/x86_64/$(cratename $repo)\">x86_64</a>" >> $d/$repo/index.html
    echo "</body></html>" >> $d/$repo/index.html
    #env CARGO_TARGET_DIR=$d xargo doc --target i686-sel4-robigalia
    env CARGO_TARGET_DIR=$d xargo doc --target x86_64-sel4-robigalia
    #env CARGO_TARGET_DIR=$d xargo doc --target arm-sel4-robigalia
    popd
done
echo "</ul>" >> $index
cat < meta/docs/template-after.html >> $index
echo "</body></html>" >> $index
