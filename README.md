# devbox

This is the "meta-repository" for Robigalia. It includes git submodules for
all of our components, and some scripts that cmr uses to make things run
nicely.

I recommend using the docker image in `runner` for development if you're not
on Linux, but there's also a Vagrantfile here, which isn't very well
maintained, but may work for you.
